# from control.scheduler.queue import Queue
# from control.scheduler.queue import CoreData
#
# from control.domain.job import Job
# from control.domain.task import Task
# from control.domain.instance_type import InstanceType
#
# from control.managers.cloud_manager import CloudManager
# from control.managers.ec2_manager import EC2Manager
#
# from control.config.ec2_config import EC2Config
#
# from control.util.linked_list import LinkedList
#
# import json
#
# path = "/home/luan/Devel/control_ckp/input/example/"
#
# job_file = path + "job.json"
# env_file = path + "env.json"
# map_file = path + "map.json"
#
#
# def load_job():
#     # Loading Job
#     try:
#         with open(job_file) as f:
#             job_json = json.load(f)
#     except Exception as e:
#         print("Error file {} ".format(job_file))
#         raise Exception(e)
#
#     return Job.from_dict(job_json)
#
#
# def load_env():
#     try:
#         with open(env_file) as f:
#             env_json = json.load(f)
#     except Exception as e:
#         print("Error file {} ".format(env_file))
#         raise Exception(e)
#
#     _env = {}
#
#     ec2 = EC2Manager()
#
#     region = EC2Config().region
#     zone = EC2Config().zone
#
#     for instance in InstanceType.from_dict(env_json):
#         # update instance price
#         if instance.provider == CloudManager.EC2:
#             price_preemptible = ec2.get_preemptible_price(instance.type, zone)[0][1]
#             price_ondemand = ec2.get_ondemand_price(instance.type,
#                                                     region=region)
#
#             instance.setup_preemptible_price(price=price_preemptible,
#                                              region=region,
#                                              zone=zone)
#
#             instance.setup_ondemand_price(price=price_ondemand,
#                                           region=region)
#
#         _env[instance.type] = instance
#
#     return _env
#
#
# def load_queues(job, env):
#     with open(map_file) as f:
#         map_json = json.load(f)
#
#     deadline_seconds = map_json['deadline']
#
#     queues = []
#
#     for key, value in map_json['instances'].items():
#         '''
#         For each instance used on the pre-scheduling
#         a queue of process is created accord with the 'map'
#         '''
#         instance_type = env[value['type']]
#
#         queue = Queue(instance_id=key,
#                       instance_type=instance_type,
#                       market=value['market'],
#                       deadline_seconds=deadline_seconds)
#
#         queue.build_queue_from_dict(job, value['map'])
#
#         queues.append(queue)
#
#     return queues
#
#
# def test_queue_to_ordered_list():
#     job = load_job()
#     env = load_env()
#     queue = load_queues(job, env)[0]
#
#     list_of_tasks = LinkedList()
#
#     # Create the list of tasks that will be executed
#     for vcpu_key, item in queue.cores.items():
#         aux = item.head
#
#         while aux is not None:
#             task: Task = aux.data.task
#             data: CoreData = aux.data
#
#             task.update_expected_execution_time(data.start, data.end)
#
#             list_of_tasks.add_ordered(
#                 task
#             )
#
#             aux = aux.next
#
#     list_of_tasks.print_list()

import os
from time import sleep
import threading

N = 4

docker_image = 'teste'
size = 3000
time_sleep_seconds = 2

print("FASE 1 - Create containers")

for i in range(1, N + 1):
    # Criar um novo container
    
    docker_create = "docker create --name container_{} -v /home/ubuntu/exec:/home/worker/ -w /home/worker/ {} sh exec.sh {}".format(
        i, docker_image, size)
    os.system(docker_create)

print("FASE 2 - Start container")
# Fase 2 - Start container




for i in range(N):
    docker_start = "docker start container_{}".format(i)
    os.system(docker_start)

lock = threading.Lock
def checkpoint_recording():
    j = 0
    while j < 4:
        sleep(time_sleep_seconds)
        lock.acquire()
        lock.release()

        docker_checkpoint = "time docker checkpoint create --checkpoint-dir=/home/ubuntu/exec/checkpoint/ --leave-running=true container_{} checkpoint{} \n".format(
            threading.current_thread().name.replace('Thread-', ''), j)
        j = j + 1

        os.system(docker_checkpoint)

        # qual a condição de parada?


print("FASE 3 - Checkpoint recording")
threads = []

for k in range(N):
    t = threading.Thread(target=checkpoint_recording)
    threads.append(t)

# não garante que todas as threads vão começar juntas. O ideal seria utilizar uma barreira ou semaforo.
for t in threads:
    t.start()

for t in threads:
    t.join()

print("FASE 4 - Folder clean")
clean = "sudo rm -rf /home/ubuntu/exec/checkpoint/*"
os.system(clean)

for i in range(1, N+1):
    docker_start="docker rm container_{}".format(i)
    os.system(docker_start)
# LOOP de 5 minutos.
# Gravar o checkpoint

# os.system('')

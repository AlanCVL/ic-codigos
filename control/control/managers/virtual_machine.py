from control.domain.instance_type import InstanceType

from control.managers.cloud_manager import CloudManager
from control.managers.ec2_manager import EC2Manager

from control.config.ec2 import EC2
from control.config.checkpoint import CheckPointConfig
from control.config.docker import DockerConfig
from control.config.file_system import FileSystem
from control.config.application import Application

from control.util.ssh_client import SSHClient

from datetime import datetime, timedelta

import uuid
import logging
import os


# Class to control the virtual machine in the cloud
# Parameters: Instance_type (InstanceType), market (str), primary (boolean)

# If primary flag is True, it indicates that the VM is a primary resource, i.e,
# the VM was launched to executed a primary task.
# Otherwise, if primary flag is False, the VM is a backup resource launched due to spot interruption/hibernation

class VirtualMachine:

    def __init__(self, instance_type: InstanceType, market, job_id, execution_id, volume_id=None):

        self.instance_type = instance_type
        self.market = market
        self.job_id = job_id
        self.execution_id = execution_id
        self.volume_id = volume_id

        self.create_file_system = False

        # Start cloud manager
        self.manager = EC2Manager()

        # ec2_info
        self.ec2_info = EC2()
        # checkpoint info
        self.ckp_inf = CheckPointConfig()
        # file system
        self.fileSystem = FileSystem()

        # docker config
        self.docker_conf = DockerConfig()

        self.instance_id = None
        self.instance_ip = None
        self.current_state = CloudManager.PENDING

        self.ready = False

        self.ssh: SSHClient = None  # SSH Client

        # Time tracker vars
        self.__start_time = None
        self.__end_time = None

        self.failed_to_created = False

        self.root_folder = None

    '''
        Methods to Manager the virtual Machine
    '''

    # Start the virtual machine
    # Return (boolean) True if success otherwise return False
    def deploy(self):

        # Check if a VM was already created
        if self.instance_id is None:

            logging.info("<Virtual Machine>: Deploying a new {} instance of type {}".format(self.market,
                                                                                            self.instance_type.type))

            if self.market == CloudManager.ON_DEMAND:
                self.instance_id = self.manager.create_on_demand_instance(self.instance_type.type)
            elif self.market == CloudManager.PREEMPTIBLE:
                self.instance_id = self.manager.create_preemptible_instance(self.instance_type.type,
                                                                            self.instance_type.price_preemptible)
            else:
                raise Exception("<VirtualMachine>: Invalid Market - {}:".format(self.market))

            # check if instance was create with success
            if self.instance_id is not None:
                # update start_time
                self.__start_time = datetime.now()
                self.instance_ip = self.manager.get_instance_ip(self.instance_id)

                if self.fileSystem.type == CloudManager.EBS:

                    # if there is not a volume create a new volume
                    if self.volume_id is None:

                        self.volume_id = self.manager.create_volume(
                            size=self.fileSystem.size,
                            zone=self.ec2_info.zone
                        )

                        self.create_file_system = True

                        if self.volume_id is None:
                            raise Exception("<VM> Error to create new volume")

                    logging.info("Attaching Volume {} to instance {}".format(self.instance_id, self.volume_id))
                    # attached new volume
                    # waiting until volume available
                    self.manager.wait_volume(volume_id=self.volume_id)
                    self.manager.attach_volume(
                        instance_id=self.instance_id,
                        volume_id=self.volume_id,
                        device=self.fileSystem.device
                    )

                return True

            else:
                self.instance_id = 'f-{}'.format(str(uuid.uuid4())[:8])
                self.failed_to_created = True

                return False

        # Instance was already started
        return False

    def __create_ebs(self):

        logging.info("<Virtual Machine>: ID: {} - Mounting EBS".format(self.instance_id))

        if self.create_file_system:
            cmd1 = 'sudo mkfs.ext4 /dev/xvdf'

            logging.info("<Virtual Machine>: {} {} ".format(self.instance_id, cmd1))
            self.ssh.execute_command(cmd1, output=True)

        # Mount Directory
        cmd2 = 'sudo mount /dev/xvdf {}'.format(self.fileSystem.path)
        logging.info("<Virtual Machine>: {} {} ".format(self.instance_id, cmd2))

        self.ssh.execute_command(cmd2, output=True)  # mount directory

        if self.create_file_system:
            cmd3 = 'sudo chown ubuntu:ubuntu -R {}'.format(self.fileSystem.path)
            cmd4 = 'chmod 775 -R {}'.format(self.fileSystem.path)

            logging.info("<Virtual Machine>: {} {} ".format(self.instance_id, cmd3))
            self.ssh.execute_command(cmd3, output=True)

            logging.info("<Virtual Machine>: {} {} ".format(self.instance_id, cmd4))
            self.ssh.execute_command(cmd4, output=True)

    def __create_s3(self):

        logging.info("<Virtual Machine>: ID: {} - Mounting S3FS".format(self.instance_id))

        # prepare S3FS
        cmd1 = 'echo {}:{} > $HOME/.passwd-s3fs'.format(self.ec2_info.aws_access_key_id, self.ec2_info.aws_access_key)

        cmd2 = 'sudo chmod 600 $HOME/.passwd-s3fs'

        # Mount the bucket
        cmd3 = 'sudo s3fs {} ' \
               '-o use_cache=/tmp -o allow_other -o uid={} -o gid={} -o mp_umask=002 -o multireq_max=5 {}'.format(
            self.ec2_info.bucket_name,
            self.ec2_info.vm_uid,
            self.ec2_info.vm_gid,
            self.fileSystem.path)

        logging.info("<Virtual Machine>: ID: {} {}".format(self.instance_id, cmd1))
        self.ssh.execute_command(cmd1, output=True)

        logging.info("<Virtual Machine>: ID: {} {}".format(self.instance_id, cmd2))
        self.ssh.execute_command(cmd2, output=True)

        logging.info("<Virtual Machine>: ID: {} {}".format(self.instance_id, cmd3))
        self.ssh.execute_command(cmd3, output=True)

    def __create_efs(self):
        logging.info("<Virtual Machine>: ID: {} - Mounting EFS".format(self.instance_id))

        # Mount NFS
        cmd1 = 'sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport {}:/' \
               ' {}'.format(
            self.ec2_info.fs_dns,
            self.fileSystem.path
        )

        cmd2 = 'sudo chown ubuntu:ubuntu -R {}'.format(self.fileSystem.path)

        logging.info("<Virtual Machine>: ID: {} {}".format(self.instance_id, cmd1))
        self.ssh.execute_command(cmd1, output=True)

        logging.info("<Virtual Machine>: ID: {} {}".format(self.instance_id, cmd2))
        self.ssh.execute_command(cmd2, output=True)


    def prepare_vm(self):

        if not self.failed_to_created:
            # update instance IP
            self.update_ip()
            # Start a new SSH Client
            self.ssh = SSHClient(self.instance_ip)

            # try to open the connection
            if self.ssh.open_connection():

                logging.info("<Virtual Machine>: {} Creating directory {}".format(self.instance_id,
                                                                                  self.fileSystem.path))
                # create directory
                self.ssh.execute_command('mkdir -p {}'.format(self.fileSystem.path), output=True)

                if self.fileSystem.type == CloudManager.EBS:
                    self.__create_ebs()

                elif self.fileSystem.type == CloudManager.S3:
                    self.__create_s3()

                elif self.fileSystem.type == CloudManager.EFS:
                    self.__create_efs()

                else:
                    logging.error("<Virtual Machine>: {} Storage type error".format(self.instance_id))

                    raise Exception(
                        "VM {} Storage {} not supported".format(self.instance_id, self.fileSystem.type))

                # Send daemon file
                app_info = Application()
                self.ssh.put_file(source=app_info.daemon_path,
                                  target=self.ec2_info.home_path,
                                  item=app_info.daemon_file)

                # create execution folder
                self.root_folder = os.path.join(self.fileSystem.path, '{}_{}'.format(self.job_id, self.execution_id))
                self.ssh.execute_command('mkdir -p {}'.format(self.root_folder), output=True)

                # Start Daemon
                logging.info("<Virtual Machine>: {} Starting Daemon".format(self.instance_id))

                cmd_daemon = "python3 {} " \
                             "--root_path {} --work_path {}" \
                             " --container_image {} --job_id {} --execution_id {} --instance_id {}".format(
                    os.path.join(self.ec2_info.home_path, app_info.daemon_file),
                    self.fileSystem.path, self.docker_conf.work_dir,
                    self.docker_conf.docker_image, self.job_id, self.execution_id, self.instance_id)

                cmd_screen = 'screen -dm bash -c "{}"'.format(cmd_daemon)
                logging.info(cmd_screen)

                self.ssh.execute_command(cmd_screen, output=True)


            else:
                logging.error("<Virtual Machine({}-{})>: SSH CONNECTION ERROR".format(
                    self.instance_id,
                    self.instance_type.type
                ))

                raise Exception("VM {} SSH Exeception ERROR".format(self.instance_id))



    # Terminate the virtual machine
    # and delete volume (if delete_volume = True)
    # Return True if success otherwise return False
    def terminate(self, delete_volume=True):

        status = False

        if self.state not in (CloudManager.TERMINATED, None):
            self.__end_time = datetime.now()
            status = self.manager.terminate_instance(self.instance_id)

            if delete_volume and self.volume_id is not None:
                self.manager.delete_volume(self.volume_id)

        return status

    def update_ip(self):
        self.instance_ip = self.manager.get_instance_ip(self.instance_id)

    # return the a IP's list of all running instance on the cloud provider
    def get_instances_ip(self):

        filter = {
            'status': [CloudManager.PENDING, CloudManager.RUNNING, CloudManager.HIBERNATED]
        }

        instances_id = self.manager.list_instances_id(filter)
        ip_list = []
        for id in instances_id:
            ip_list.append(self.manager.get_instance_ip(id))

        return ip_list

    # Return the current state of the virtual machine
    @property
    def state(self):
        if not self.failed_to_created:
            self.current_state = self.manager.get_instance_status(self.instance_id)
        else:
            self.current_state = CloudManager.ERROR

        if self.current_state is None:
            self.current_state = CloudManager.TERMINATED

        return self.current_state

    # Return the machine start time
    # Return None If the machine has not start
    @property
    def start_time(self):
        return self.__start_time

    # Return the uptime if the machine was started
    # Otherwise return None
    @property
    def uptime(self):
        _uptime = timedelta(seconds=0)

        # check if the VM has started
        if self.__start_time is not None:
            # check if VM has terminated
            if self.__end_time is not None:
                _uptime = self.__end_time - self.__start_time
            else:
                _uptime = datetime.now() - self.__start_time

            # remove the hibernation_duration
            _uptime = max(_uptime, timedelta(seconds=0))

        return _uptime

    # Return the shutdown time if the machine was terminated
    # Otherwise return None
    @property
    def end_time(self):
        return self.__end_time

    @property
    def price(self):
        if self.market == CloudManager.PREEMPTIBLE:
            return self.manager.get_preemptible_price(self.instance_type.type, self.ec2_info.zone)[0][1]

        else:
            return self.instance_type.price_ondemand

from control.config.config import Config


class SocketConfig(Config):
    _key = 'socket'

    @property
    def port(self):
        return int(self.get_property(self._key, 'port'))

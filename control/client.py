#!/usr/bin/env python
from control.managers.schedule_manager import ScheduleManager

from control.config.logging import LoggingConfig
from control.config.input import Input
from control.config.database import DataBaseConfig

from control.generators.primary_map import PrimaryMap

from control.scheduler.primary_scheduler import PrimaryScheduler

from distutils.util import strtobool

import logging

import traceback

import argparse

import control.util.backup_database as dump
from control.util.mail_me import MailMe
from control.util.recreate_database import RecreateDatabase
import datetime


def __prepare_logging(log_file):
    log_conf = LoggingConfig()

    logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    rootLogger = logging.getLogger()
    rootLogger.setLevel(log_conf.level)

    file = log_file
    if file is None:
        file = log_conf.log_file

    fileHandler = logging.FileHandler("{0}/{1}.log".format(log_conf.path, file))
    fileHandler.setFormatter(logFormatter)
    rootLogger.addHandler(fileHandler)

    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    rootLogger.addHandler(consoleHandler)


def __prepare_input(args):
    # get input info and generate input path
    path = args.input_path
    job_file = args.job_file
    env_file = args.env_file
    map_file = args.map_file

    input_info = Input()

    if path is None:
        path = input_info.path
    if job_file is None:
        job_file = input_info.job_file
    if env_file is None:
        env_file = input_info.env_file
    if map_file is None:
        map_file = input_info.map_file

    env_file = path + env_file
    job_file = path + job_file
    map_file = path + map_file

    return env_file, job_file, map_file


def __notification(status, job_file, env_file, map_file):
    # Sending email
    email_pwd = 'lhfitfxpbrtctytf'
    email_user = 'luanteylo@gmail.com'

    try:
        # mail me
        mail = MailMe(user=email_user,
                      pwd=email_pwd)

        subject = "Control Execution Status: {}".format(status)
        msg = "Status: {}\n" \
              "JOB: {}\n" \
              "Map: {}\n" \
              "Env: {}\n" \
              "Finish time: {}".format(status, job_file, map_file, env_file, datetime.datetime.now())

        mail.send_email(recipient=email_user,
                        subject=subject,
                        body=msg)
    except Exception as e:
        logging.error("Error to send msg")
        logging.error(e)


def __call_control(args):
    env_file, job_file, map_file = __prepare_input(args)

    try:

        __prepare_logging(args.log_file)

        logging.info("Executing control application.")

        manager = ScheduleManager(job_file=job_file,
                                  env_file=env_file,
                                  map_file=map_file,
                                  deadline=args.deadline,
                                  app_local_path=args.app_path,
                                  ac_size=args.ac_size,
                                  revocation_rate=args.revocation_rate,
                                  resume_rate=args.resume_rate,
                                  parrallel_factor=args.parrallel_factor,
                                  checkpoint_period=args.checkpoint_period)

        manager.start_execution()

        status = "SUCCESS"

    except Exception:
        print(traceback.format_exc())
        status = "ERROR"

    # check if the user want be notified at the end of the execution
    if args.notify:
        __notification(status=status,
                       map_file=map_file,
                       env_file=env_file,
                       job_file=job_file)

    if args.dump:
        logging.info("Backup Database..")
        dump.dump_db()
        logging.info("Backup finished...")


def __call_primary_scheduling(args):
    __prepare_logging(args.log_file)
    logging.info("Executing primary scheduling.")

    if args.deadline is None:
        logging.error("Argument error: Deadline required to generates primary MAP")
        raise Exception("Argument error")

    logging.info("Selection method: {}".format(args.vm_selection))

    env_file, job_file, map_file = __prepare_input(args)

    generator = PrimaryMap(
        env_file=env_file,
        job_file=job_file,
        map_file=map_file,
        deadline_in_seconds=int(args.deadline),
        selection=args.vm_selection,
        verbose=args.verbose
    )

    generator.create_map()


def __clean_database(args):
    if strtobool(input("Are you sure that you want to clean the database? (yes or no): ")):
        RecreateDatabase.execute()
        logging.info("Control database was recreated")


def main():
    parser = argparse.ArgumentParser(description='Dynamic Scheduling Control Application')

    parser.add_argument('--input_path', type=str, default=None)
    parser.add_argument('--job_file', type=str, default=None)
    parser.add_argument('--env_file', type=str, default=None)
    parser.add_argument('--map_file', type=str, default=None)
    parser.add_argument('--deadline', type=int, default=None)

    parser.add_argument('--log_file', type=str, default=None)

    parser.add_argument('--app_path', type=str, default=None)

    parser.add_argument('--resume_rate', type=float, default=None)
    parser.add_argument('--revocation_rate', type=float, default=None)

    parser.add_argument('--ac_size', help="size of the initial Allocation Cycle", type=int, default=300)

    parser.add_argument('--notify', help='Send an email to notify the end of the execution (control mode)',
                        action='store_true', default=False)

    parser.add_argument('--dump', help='dump database',
                        action='store_true', default=False)

    parser.add_argument('--verbose', help='Print extra info during the primary scheduling',
                        action='store_true', default=False)

    parser.add_argument('--parrallel_factor', type=int, default=None)

    parser.add_argument('--checkpoint_period', type=int, default=None)

    options_map = {
        'control': __call_control,
        'map': __call_primary_scheduling,
        'clean_db': __clean_database,
    }

    parser.add_argument('command', choices=options_map.keys())

    args = parser.parse_args()

    func = options_map[args.command]

    func(args=args)


if __name__ == "__main__":
    main()

===============================
control
===============================


.. image:: https://img.shields.io/pypi/v/control.svg
        :target: https://pypi.python.org/pypi/control

.. image:: https://img.shields.io/travis/luanteylo/control.svg
        :target: https://travis-ci.org/luanteylo/control

.. image:: https://readthedocs.org/projects/control/badge/?version=latest
        :target: https://control.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/luanteylo/control/shield.svg
     :target: https://pyup.io/repos/github/luanteylo/control/
     :alt: Updates


A control package used in the dynamic scheduling project


* Free software: MIT license
* Documentation: https://control.readthedocs.io.


Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage


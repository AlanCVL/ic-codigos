# Generate the primary MAP

# input:  path - the path where the files are and where the primary map will be writen
#         env_file - json file that contains VMs' information
#         job_file - json file that contains application's information
#         map_name - the name of the output file
# output: a json file with the primary map

from control.domain.instance_type import InstanceType
from control.domain.job import Job

from control.scheduler.primary_scheduler import PrimaryScheduler

import json

from datetime import timedelta


class PrimaryMap:

    def __init__(self, env_file, job_file, map_file, deadline_in_seconds: int, selection: str, verbose=False):
        self.env_file = env_file
        self.job_file = job_file
        self.map_file = map_file

        self.selection = selection

        self.deadline = timedelta(seconds=deadline_in_seconds)

        self.verbose = verbose

        # a dict with all instances that can be used by the scheduling
        self.env = None
        # a job class object with the job information
        self.job = None

        self.__load_files()

    def __load_files(self):
        # loading ENV
        with open(self.env_file) as f:
            env_json = json.load(f)

        self.env = {}

        for instance in InstanceType.from_dict(env_json):
            self.env[instance.type] = instance

        # load JOB
        with open(self.job_file) as f:
            job_json = json.load(f)

        self.job = Job.from_dict(job_json)

    def create_map(self):
        scheduler = PrimaryScheduler(job=self.job,
                                     instances=self.env,
                                     deadline=self.deadline,
                                     selection=self.selection)

        scheduler.create_map()
        scheduler.write_json(self.map_file)

        if self.verbose:
            scheduler.print_map()
        scheduler.print_info()

from control.managers.cloud_manager import CloudManager
from control.managers.virtual_machine import VirtualMachine
from control.managers.daemon_manager import Daemon

from control.domain.task import Task

from control.scheduler.queue import Queue

from control.util.linked_list import LinkedList
from control.util.event import Event

from control.socket.communicator import Communicator

from control.config.checkpoint import CheckPointConfig
from control.config.socket import SocketConfig
from control.config.application import Application

from control.repository.postgres_repo import PostgresRepo
from control.repository.postgres_objects import Execution as ExecutionRepo
from control.repository.postgres_objects import Instance as InstanceRepo
from control.repository.postgres_objects import InstanceStatus as InstanceStatusRepo

from typing import List
import threading
import time
import logging
import os

from datetime import datetime, timedelta

from zope.event import notify


class Executor:

    def __init__(self, task: Task, vm: VirtualMachine, ckp_barrier, execution_id):
        self.ckp_barrier = ckp_barrier
        self.num_checkpoints = 0

        self.task = task
        self.vm = vm
        self.execution_id = execution_id

        # Execution Status
        self.status = Task.WAITING

        # socket.communicator
        # used to send commands to the ec2 instance
        self.communicator = Communicator(host=self.vm.instance_ip, port=SocketConfig().port)

        # Task Track INFO
        self.max_memory = 0.0

        # flags
        # used to abort the execution loop
        self.stop_signal = False

        # print("task command:", self.task_command)

        # checkpoint tracker
        self.with_checkpoint = CheckPointConfig().with_checkpoint
        self.last_checkpoint_datetime = None

        self.ckp_durations = []

        self.app_info = Application()

        self.thread = threading.Thread(target=self.__run, daemon=True)

    def update_status_table(self):
        """
        Update Execution Table
        Call if task status change
        """

        repo = PostgresRepo()

        # Update Execution Status Table
        repo.add_execution(
            ExecutionRepo(
                execution_id=self.execution_id,
                job_id=self.task.job_id,
                task_id=self.task.task_id,
                instance_id=self.vm.instance_id,
                timestamp=datetime.now(),
                status=self.status,
                avg_memory=self.max_memory
            )
        )

        repo.close_session()

    def __run(self):
        # START task execution

        action = Daemon.START

        if self.task.has_checkpoint:
            action = Daemon.RESTART

        # Send files to VM
        if action == Daemon.START:
            # send files
            source = os.path.join(self.app_info.app_local_path, str(self.task.task_id))
            target = os.path.join(self.vm.root_folder, str(self.task.task_id))

            try:
                # create target folder
                self.vm.ssh.execute_command('mkdir -p {}'.format(target))
                self.vm.ssh.put_dir(source, target)
            except Exception as e:
                logging.error(e)
                self.__stopped(Task.ERROR)
                return

        # Try to start the task,
        try:
            self.communicator.send(action=action, value=self.dict_info)
            self.last_checkpoint_datetime = datetime.now()
        except Exception as e:
            logging.error(e)
            self.__stopped(Task.ERROR)
            return

        # if task was started with success
        # start execution loop
        if self.communicator.response['result']['status'] == 'success':

            # logging.info(self.communicator.response['result']['value'])

            if action == Daemon.START:
                self.status = Task.EXECUTING
            else:
                self.status = Task.RESTARTED

            self.update_status_table()

            # start task execution Loop
            while (self.status == Task.EXECUTING or self.status == Task.RESTARTED) and not self.stop_signal:

                try:
                    docker_status, exit_code = self.__get_task_status()
                    usage = self.__get_task_usage()

                    period = datetime.now() - self.last_checkpoint_datetime
                    if self.with_checkpoint and period > timedelta(seconds=self.task.checkpoint_period):
                        # if self.num_checkpoints < 3:  # TODO Task 5000 test
                        self.__checkpoint()

                except Exception as e:
                    logging.error(e)
                    self.__stopped(Task.ERROR)
                    return

                # update memory usage
                if usage is not None and 'memory' in usage:
                    current_mem = self.__to_megabyte(usage['memory'])
                    if current_mem > self.max_memory:
                        self.max_memory = current_mem

                if docker_status is not None and docker_status == 'exited':
                    status = Task.FINISHED

                    if exit_code != '0':
                        status = Task.RUNTIME_ERROR

                    self.__stopped(status)
                    return

                time.sleep(1)

        # if kill signal than checkpoint task (SIMULATION)
        # if self.stop_signal:
        #
        #     # check is task is running
        #     try:
        #         docker_status, exit_code = self.__get_task_status()
        #         if docker_status is not None and docker_status == 'running':
        #             self.__checkpoint(stop_task=True)  # Checkpoint and stop task
        #             self.__stopped(self.HIBERNATED)
        #         else:
        #             self.__stopped(self.FINISHED)
        #
        #     except Exception as e:
        #         logging.error(e)
        #         self.__stopped(self.ERROR)
        #
        #     return

        self.__stopped(Task.ERROR)

    def __stopped(self, status):
        # update execution time

        # if task had Migrated, not to do
        if self.status == Task.MIGRATED:
            return

        self.status = status

        logging.info(
            "Task {} VM {} - Num checkpoint: {} Avg ckp time: {}".format(
                self.task.task_id,
                self.vm.instance_id,
                self.num_checkpoints,
                self.__avg_ckp()
            ))

        self.update_status_table()

    def __checkpoint(self, stop_task=False):

        # self.ckp_barrier[0].wait()

        for i in range(3):
            try:

                action = Daemon.CHECKPOINT_STOP if stop_task else Daemon.CHECKPOINT

                logging.info(
                    "<Executor>: Checkpointing task {} on VM {}".format(self.task.task_id, self.vm.instance_id))

                start_ckp = datetime.now()
                self.communicator.send(action, value=self.dict_info)

                if self.communicator.response['result']['status'] == 'success':
                    end_ckp = datetime.now()

                    self.ckp_durations.append(end_ckp - start_ckp)

                    logging.info("<Executor> Task {} Checkpointed with success. Time: {}".format(self.task.task_id,
                                                                                                 end_ckp - start_ckp))
                    self.task.has_checkpoint = True
                    # update last_checkpoint
                    self.last_checkpoint_datetime = end_ckp
                    self.num_checkpoints += 1

                return
            except:
                pass

        raise Exception("Checkpoint error")

    def __get_task_status(self):

        for i in range(3):

            try:

                self.communicator.send(action=Daemon.STATUS,
                                       value=self.dict_info)

                result = self.communicator.response['result']

                docker_status = None
                exit_code = None

                if result['status'] == 'success':
                    docker_status = result['value']['status']
                    exit_code = result['value']['exit_code']

                return docker_status, exit_code

            except:
                logging.error("Get task {} Status {}/3".format(self.task.task_id, i + 1))
                time.sleep(1)

        raise Exception("Get task status error")

    def __get_task_usage(self):
        for i in range(3):
            try:
                self.communicator.send(action=Daemon.USAGE,
                                       value=self.dict_info)

                result = self.communicator.response['result']

                usage = None

                if result['status'] == 'success':
                    usage = result['value']

                return usage
            except:
                logging.error("Get task {} Usage {}/3".format(self.task.task_id, i + 1))
                time.sleep(1)

        raise Exception("Get task usage error")

    def __to_megabyte(self, str):

        pos = str.find('MiB')

        if pos == -1:
            pos = str.find('GiB')
        if pos == -1:
            pos = str.find('KiB')
        if pos == -1:
            pos = str.find('B')

        memory = float(str[:pos])
        index = str[pos:]

        to_megabyte = {
            "GiB": 1073.742,
            "MiB": 1.049,
            "B": 1e+6,
            "KiB": 976.562
        }

        return to_megabyte[index] * memory

    def __avg_ckp(self):
        if len(self.ckp_durations) > 0:
            sum = timedelta(seconds=0)
            for it in self.ckp_durations:
                sum += it
                logging.info("checkpoint task {}: duration: {}".format(self.task.task_id, it))
            return sum / len(self.ckp_durations)
        else:
            return 0.0

    @property
    def dict_info(self):

        info = {
            "task_id": self.task.task_id,
            "command": self.task.command,
        }

        return info


class Dispatcher:
    error_tasks: List[Task]
    finished_tasks: List[Task]
    hibernated_tasks: List[Task]
    waiting_tasks: List[Task]
    running_tasks: List[Task]

    running_executors: List[Executor]
    finished_executors: List[Executor]

    # checkpoint supported formulations
    FORMULATION_YOUNG = 'young'
    FORMULATION_SHENG = 'sheng'
    FORMULATION_FIXED = 'fixed'

    def __init__(self, vm: VirtualMachine, queue: Queue, job, execution_id, ckp_barrier, checkpoint_period, from_migration=False):
        self.checkpoint_period = checkpoint_period
        self.ckp_barrier = ckp_barrier

        self.vm = vm  # Class that control a Virtual machine on the cloud
        self.queue = queue  # Class with the scheduling plan
        self.job = job  # Class with all the input information about the job
        self.execution_id = execution_id

        # Control Flags
        self.working = False
        self.from_migration = from_migration

        '''
        List that determine the execution order of the
        tasks that will be executed in that dispatcher
        '''
        self.list_of_tasks = LinkedList()

        # current tasks status
        self.waiting_tasks = []
        self.finished_tasks = []
        self.error_tasks = []
        self.hibernated_tasks = []
        self.running_tasks = []

        # current executor status
        self.running_executors = []
        self.finished_executors = []

        # threading event to waiting for tasks to execute
        self.waiting_work = threading.Event()
        self.semaphore = threading.Semaphore()

        self.main_thread = threading.Thread(target=self.__execution_loop, daemon=True)

        self.repo = PostgresRepo()
        self.least_status = None

        # self.stop_period = None

        # Checkpoint dict time
        self.checkpoint_info = CheckPointConfig()
        if not self.from_migration:
            self.__create_ordered_list()

    def __update_instance_status_table(self, state=None):
        """
        Update Instance Status table
        """
        if state is None:
            state = self.vm.state

        if self.least_status is None or self.least_status != state:
            # Update Instance_status Table
            self.repo.add_instance_status(
                InstanceStatusRepo(
                    instance_id=self.vm.instance_id,
                    timestamp=datetime.now(),
                    status=state
                )
            )

            self.least_status = self.vm.state

    def __update_waiting_tasks(self):
        for task in self.waiting_tasks:
            # Update Execution Status Table
            self.repo.add_execution(
                ExecutionRepo(
                    execution_id=self.execution_id,
                    job_id=task.job_id,
                    task_id=task.task_id,
                    instance_id=self.vm.instance_id,
                    timestamp=datetime.now(),
                    status=Task.WAITING,
                    avg_memory=0.0
                )
            )

    def __create_ordered_list(self):

        logging.info('Loading dispatcher queue to instance type: "{}" Logical ID: {}'.format(
            self.queue.instance_type.type,
            self.queue.instance_id
        ))

        self.semaphore.acquire()

        # Create the list of tasks that will be executed
        for vcpu_key, item in self.queue.cores.items():
            aux = item.head

            while aux is not None:
                task: Task = aux.data.task

                if self.checkpoint_info.with_checkpoint:
                    # define checkpoint formulation
                    if self.checkpoint_info.formulation == Dispatcher.FORMULATION_FIXED:
                        if self.checkpoint_period is not None:
                            task.checkpoint_period = self.checkpoint_period
                        else:
                            task.checkpoint_period = self.checkpoint_info.period
                    else:
                        raise Exception(
                            "ERROR! Formulation '{}' not supported".format(self.checkpoint_info.formulation))

                    logging.info("Task {} - checkpoint period: '{}' formulation: '{}'".format(
                        task.task_id,
                        task.checkpoint_period,
                        self.checkpoint_info.formulation)
                    )

                self.list_of_tasks.add_ordered(
                    task
                )

                self.waiting_tasks.append(task)

                aux = aux.next

        self.semaphore.release()

    def migrate_tasks(self, tasks: List[Task]):

        for task in tasks:
            self.list_of_tasks.add_ordered(task)
            self.waiting_tasks.append(task)

    def __notify(self, value):

        kwargs = {'instance_id': self.vm.instance_id,
                  'dispatcher': self,
                  'running': self.running_tasks,
                  'finished': self.finished_tasks,
                  'waiting': self.waiting_tasks,
                  'hibernated': self.hibernated_tasks}

        notify(
            Event(
                event_type=Event.INSTANCE_EVENT,
                value=value,
                **kwargs
            )
        )

    def __can_execute(self, task: Task):
        can_execute = True

        for executor in self.running_executors:
            r_task = executor.task

            if r_task.expected_end <= task.expected_start:
                can_execute = False

        return can_execute

    def __update_running_executors(self):

        new_list_executors = []
        new_list_tasks = []

        for r in self.running_executors:

            if r.status == Task.EXECUTING or r.status == Task.WAITING or r.status == Task.RESTARTED:
                new_list_executors.append(r)
                new_list_tasks.append(r.task)
            else:
                if r.status == Task.FINISHED:
                    self.finished_tasks.append(r.task)

                elif r.status == Task.ERROR:
                    self.error_tasks.append(r.task)

                elif r.status == Task.HIBERNATED:
                    self.hibernated_tasks.append(r.task)

                self.finished_executors.append(r)

        self.running_tasks = new_list_tasks
        self.running_executors = new_list_executors

    def __test_daemon(self):

        # logging.info("Testing Daemon instance {}".format(self.vm.instance_id))
        for i in range(5):
            time.sleep(5)
            try:

                communicator = Communicator(host=self.vm.instance_ip, port=SocketConfig().port)
                communicator.send(action=Daemon.TEST,
                                  value={'task_id': None,
                                         'command': None}
                                  )

                if communicator.response['result']['status'] == 'success':
                    return

            except Exception as e:
                logging.error(e)

            # time.sleep(5)

    def __execution_loop(self):

        # Start the VM in the cloud
        status = self.vm.deploy()

        # update instance_repo
        self.repo.add_instance(
            InstanceRepo(
                id=self.vm.instance_id,
                type=self.vm.instance_type.type,
                region=self.vm.instance_type.region,
                zone=self.vm.instance_type.zone,
                market=self.vm.market,
                price=self.vm.price
            )
        )
        self.__update_instance_status_table()

        if status:

            self.vm.prepare_vm()
            self.__test_daemon()

            self.__update_waiting_tasks()

            self.working = True


            while self.working:
                # waiting for work
                self.waiting_work.wait()

                self.waiting_work.clear()
                if not self.working:
                    break

                # execution loop
                self.semaphore.acquire()
                task: Task = self.list_of_tasks.pop()
                self.semaphore.release()

                while task is not None or len(self.running_executors) > 0:

                    self.__update_instance_status_table()

                    # start Execution when instance is RUNNING
                    if self.vm.state == CloudManager.RUNNING:

                        # if self.checkpoint_info.stop and not self.from_migration:
                        #
                        #     period = datetime.now() - start_period
                        #     if period > timedelta(seconds=self.stop_period):
                        #         ec2 = EC2Manager()
                        #         ec2.terminate_instance(self.vm.instance_id, wait=False)

                        self.semaphore.acquire()
                        # check running tasks
                        self.__update_running_executors()

                        # check if the next task can be executed
                        if task is not None and self.__can_execute(task):
                            # execute task
                            self.waiting_tasks.remove(task)
                            self.running_tasks.append(task)

                            # create a executor and start task
                            executor = Executor(
                                task=task,
                                vm=self.vm,
                                execution_id=self.execution_id,
                                ckp_barrier=self.ckp_barrier
                            )
                            # start the executor loop to execute the task
                            executor.thread.start()
                            self.vm.ready = True
                            self.running_executors.append(executor)

                            # Get the next task
                            task = self.list_of_tasks.pop()

                        self.semaphore.release()

                    # Error: instance was not deployed or was terminated
                    elif self.vm.state in (CloudManager.ERROR, CloudManager.SHUTTING_DOWN, CloudManager.TERMINATED):
                        # VM was not created, raise a event
                        self.__notify(CloudManager.TERMINATED)

                        break

                    elif self.vm.state == CloudManager.HIBERNATED:
                        # Raise Hibernated Event
                        self.__notify(CloudManager.HIBERNATED)

                        break

                # Raise IDLE EVENT
                if self.vm.state == CloudManager.RUNNING:
                    self.__update_instance_status_table(state=CloudManager.IDLE)
                    self.__notify(CloudManager.IDLE)

            if self.vm.state == CloudManager.RUNNING:
                self.vm.terminate()

            self.__update_instance_status_table()

        else:
            # Error to start VM
            logging.error("VM {} Was not started".format(self.vm.instance_type.type))
            self.__notify(CloudManager.ERROR)

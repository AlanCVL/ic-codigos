import json

from control.domain.instance_type import InstanceType
from control.domain.task import Task
from control.domain.job import Job

from control.managers.cloud_manager import CloudManager
from control.managers.ec2_manager import EC2Manager

from control.config.ec2 import EC2

from control.scheduler.queue import Queue

from typing import List, Dict

from datetime import timedelta

import uuid


class PrimaryScheduler:

    def __init__(self, job: Job, instances: Dict[str, InstanceType], deadline: timedelta):
        self.job = job
        self.tasks = job.tasks
        self.instances = instances
        self.deadline = deadline

        self.deadline = self.deadline

        self.expected_makespan_seconds = 0.0
        self.expected_cost = 0.0
        self.expected_cost_ondemand = 0.0
        self.queues: List[Queue] = []

        self.count_dict = {}

    def create_map(self):

        self.__get_prices()
        instances = []
        tasks_list = []

        # create list of instances
        for key, instance in self.instances.items():
            instances.append(instance)

        # Create a list of tasks
        for key, task in self.tasks.items():
            tasks_list.append(task)

        print("Task list size: ", len(tasks_list))

        self.__primary_heuristic(instances, tasks_list)

    def __primary_heuristic(self, instances: List[InstanceType], tasks_list: List[Task]):

        # START SCHEDULING, To each Task do
        for instance in instances:
            # check the limit of instances and if migration restriction will be respected
            queue = Queue(instance_id=uuid.uuid4(),
                          instance_type=instance,
                          market=CloudManager.PREEMPTIBLE,
                          deadline_seconds=self.deadline)

            count = 0

            while count < 10:
                task = tasks_list.pop()

                task.memory = instance.memory
                queue.insert(task)
                self.queues.append(queue)
                count += 1

        # GET COST AND MAKESPAN
        for queue in self.queues:
            if self.expected_makespan_seconds < queue.makespan_seconds:
                self.expected_makespan_seconds = queue.makespan_seconds

            self.expected_cost += queue.expected_cost
            self.expected_cost_ondemand += queue.expected_cost_ondemand

    # get the prices of the instances
    def __get_prices(self):
        ec2 = EC2Manager()

        zone = EC2().zone
        region = EC2().region

        for instance in self.instances.values():
            instance.setup_ondemand_price(
                price=ec2.get_ondemand_price(instance_type=instance.type, region=region),
                region=region
            )

            instance.setup_preemptible_price(
                price=ec2.get_preemptible_price(instance_type=instance.type, zone=zone)[0][1],
                region=region,
                zone=zone
            )

    def print_map(self):
        print("\n")
        for queue in self.queues:
            queue.print_queue()
        print("\n")

    def print_info(self):
        print("Expected cost: ", self.expected_cost)
        print("Expected cost on-demand only", self.expected_cost_ondemand)
        print("Expected Makespan: ", timedelta(seconds=self.expected_makespan_seconds))
        print(50 * "#" + "\n")

    def write_json(self, file_output):
        # build a map
        dict = {"job_id": self.job.job_id,
                "job_name": self.job.job_name,
                "expected_makespan": self.expected_makespan_seconds,
                "expected_cost": self.expected_cost,
                "deadline": self.deadline.seconds,
                "instances": {}
                }

        for queue in self.queues:
            if queue.market == CloudManager.PREEMPTIBLE:
                price = queue.instance_type.price_preemptible
            else:
                price = queue.instance_type.price_ondemand

            key = "id{}".format(queue.instance_id)
            dict['instances'][key] = {"type": queue.instance_type.type,
                                      "market": queue.market,
                                      "price": price,
                                      "map": queue.to_dict()}

        with open(file_output, "w") as fp:
            json.dump(dict, fp, sort_keys=True, indent=4, default=str)

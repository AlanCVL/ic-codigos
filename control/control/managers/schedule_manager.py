import json

import time

from datetime import timedelta, datetime

import logging
from typing import List, Dict

from zope.event import subscribers

from control.domain.job import Job
from control.domain.instance_type import InstanceType
from control.domain.task import Task

from control.config.application import Application
from control.config.input import Input
from control.config.ec2 import EC2
from control.config.simulation import SimulationConfig
from control.config.checkpoint import CheckPointConfig

from control.managers.virtual_machine import VirtualMachine
from control.managers.dispatcher import Dispatcher
from control.managers.cloud_manager import CloudManager
from control.managers.ec2_manager import EC2Manager

from control.simulators.status_simulator import RevocationSim
from control.simulators.status_simulator import HibernationSim
from control.simulators.status_simulator import Sim

from control.repository.postgres_repo import PostgresRepo
from control.repository.postgres_objects import Job as JobRepo
from control.repository.postgres_objects import Task as TaskRepo
from control.repository.postgres_objects import InstanceType as InstanceTypeRepo

from control.scheduler.queue import Queue

import threading


class ScheduleManager:
    hibernated_dispatcher: List[Dispatcher]
    terminated_dispatcher: List[Dispatcher]
    idle_dispatchers: List[Dispatcher]
    working_dispatchers: List[Dispatcher]
    hibernating_dispatcher: List[Dispatcher]

    env: Dict[str, InstanceType]
    job: Job

    def __init__(self, job_file, env_file, map_file, deadline, app_local_path, ac_size, revocation_rate, resume_rate,
                 parrallel_factor, checkpoint_period):

        # define execution_id on load_env()
        self.execution_id = None
        # read deadline on load_map()
        self.deadline_seconds = deadline
        # read expected_makespan on build_dispatcher()
        self.expected_makespan_seconds = None

        self.ckp_period = checkpoint_period

        if parrallel_factor is None:
            parrallel_factor = CheckPointConfig().parallel_factor

        self.ckp_barrier = threading.Barrier(parrallel_factor)

        # Prepare Application local path
        self.app_local_path = app_local_path

        if self.app_local_path is None:
            self.app_local_path = Application().app_local_path

        self.ac_size = ac_size
        self.revocation_rate = revocation_rate
        self.resume_rate = resume_rate

        self.sim_conf = SimulationConfig()
        '''
           If the execution has simulation
           Prepare the simulation environment
        '''
        if self.sim_conf.with_simulation:

            if self.revocation_rate is None:
                self.revocation_rate = self.sim_conf.revocation_rate

            if self.resume_rate is None:
                self.resume_rate = self.sim_conf.resume_rate

            # start simulator
            if self.sim_conf.sim_type == Sim.REVOCATION_SIM:
                self.simulator = RevocationSim(self.revocation_rate)
            elif self.sim_conf.sim_type == Sim.HIBERNATION_SIM:
                self.simulator = HibernationSim(self.revocation_rate, self.resume_rate)
            else:
                raise Exception("Simulator {} Not Found. Check HADS' setup file".format(self.sim_conf.sim_type))

        # resource pools
        self.working_dispatchers = []
        self.idle_dispatchers = []
        self.terminated_dispatcher = []
        self.hibernated_dispatcher = []
        self.hibernating_dispatcher = []

        # Vars Datetime to keep track of global execution time
        self.start_timestamp = None
        self.end_timestamp = None
        self.deadline_timestamp = None
        self.expected_makespan_timestamp = None
        self.elapsed_time = None

        self.repo = PostgresRepo()

        # instances able to be used
        self.instances = []

        self.input = Input()

        # Semaphore
        self.semaphore = threading.Semaphore()

        self.job_file = job_file
        self.env_file = env_file
        self.map_file = map_file

        logging.info("Loading input Files")
        logging.info("Job: {}".format(self.job_file))
        logging.info("Env: {}".format(self.env_file))
        logging.info("Map: {}".format(self.map_file))

        '''
        Class domain.job contains all tasks that will be executed
        and the information  about the job
        '''
        self.job = self.__load_job()

        '''
        Dictionary with the domain.instance_type that can be used in the execution
        Read from env.json
        '''
        self.env = self.__load_env()

        '''
        Build the initial dispatchers
        The class Dispatcher is responsible to manager the execution steps
        '''
        self.__build_dispatchers()

        # Prepare the control database and the folders structure in S3
        self.__prepare_execution()

    # PRE-EXECUTION FUNCTIONS
    def __load_job(self):
        try:
            with open(self.job_file) as f:
                job_json = json.load(f)
        except Exception as e:
            logging.error("Error file {} ".format(self.job_file))
            raise Exception(e)

        return Job.from_dict(job_json)

    def __load_env(self):
        try:
            with open(self.env_file) as f:
                env_json = json.load(f)
        except Exception as e:
            logging.error("Error file {} ".format(self.env_file))
            raise Exception(e)

        _env = {}

        ec2 = EC2Manager()

        region = EC2().region
        zone = EC2().zone

        for instance in InstanceType.from_dict(env_json):
            # update instance price
            prices = ec2.get_preemptible_price(instance.type, zone)

            if len(prices) > 0:
                price_preemptible = prices[0][1]
            else:
                price_preemptible = 0.0

            price_ondemand = ec2.get_ondemand_price(instance.type, region=region)

            instance.setup_preemptible_price(price=price_preemptible, region=region, zone=zone)

            instance.setup_ondemand_price(price=price_ondemand, region=region)

            _env[instance.type] = instance

            self.instances.append(instance)

        return _env

    def __build_dispatchers(self):

        # Define the execution id
        # get the last execution id
        try:
            row = self.repo.get_execution(
                filter={
                    'job_id': self.job.job_id,
                    'limit': 1,
                    'order': 'desc'
                }
            )

            if len(row) == 0:
                self.execution_id = 0
            else:
                # get least execution ID
                self.execution_id = row[0].execution_id + 1

            with open(self.map_file) as f:
                map_json = json.load(f)

        except FileNotFoundError:
            logging.error("Error file {} ".format(self.map_file))
            raise FileNotFoundError

        except Exception:
            logging.error("<ScheduleManager>: Error database not found")
            raise Exception

        if self.deadline_seconds is None:
            self.deadline_seconds = map_json['deadline']

        self.expected_makespan_seconds = float(map_json['expected_makespan'])

        for key, value in map_json['instances'].items():
            '''
            For each instance used on the pre-scheduling
            a queue of process is created accord with the 'map'
            '''
            instance_type = self.env[value['type']]

            queue = Queue(instance_id=key,
                          instance_type=instance_type,
                          market=value['market'],
                          deadline_seconds=self.deadline_seconds)

            queue.build_queue_from_dict(self.job, value['map'])

            # Create the Vm that will be used by the dispatcher
            vm = VirtualMachine(
                instance_type=instance_type,
                market=value['market'],
                execution_id=self.execution_id,
                job_id=self.job.job_id
            )

            # than a dispatcher, that will execute the tasks, is create

            dispatcher = Dispatcher(
                vm=vm,
                queue=queue,
                job=self.job,
                execution_id=self.execution_id,
                ckp_barrier=[self.ckp_barrier],
                checkpoint_period=self.ckp_period
            )

            # check if the VM need to be register on the simulator
            if self.sim_conf.with_simulation and vm.market == CloudManager.PREEMPTIBLE:
                self.simulator.register_vm(vm)

            self.semaphore.acquire()

            self.working_dispatchers.append(dispatcher)

            self.semaphore.release()

    def __prepare_execution(self):
        """
           Prepare control database and all directories to start the execution process
        """

        # get job from control database
        jobs_repo = self.repo.get_jobs(
            filter={
                'job_id': self.job.job_id
            }
        )

        # Check if Job is already in the database
        if len(jobs_repo) == 0:
            # add job to database
            self.__add_job_to_database()
        else:
            # Job is already in database
            # Check job and Instances consistency
            logging.info("Checking database consistency...")

            job_repo = jobs_repo[0]

            assert job_repo.name == self.job.job_name, "Consistency error (job name): {} <> {}".format(
                job_repo.name, self.job.job_name)

            assert job_repo.description == self.job.description, "Consistency error (job description): {} <> {} ".format(
                job_repo.description, self.job.description)

            tasks_repo = job_repo.tasks.all()

            assert len(tasks_repo) == len(self.job.tasks), "Consistency error (number of tasks): {} <> {} ".format(
                len(tasks_repo), len(self.job.tasks)
            )

            # check tasks consistency
            for t in tasks_repo:
                assert str(t.task_id) in self.job.tasks, "Consistency error (task id): {}".format(
                    t.task_id)

                task = self.job.tasks[str(t.task_id)]

                assert task.memory == t.memory, "Consistency error (task {} memory): {} <> {} ".format(
                    task.task_id, t.memory, task.memory)

                assert task.command == t.command, "Consistency error (task {} command): {} <> {} ".format(
                    task.task_id, t.command, task.command)

                assert task.io == t.io, "Consistency error (task {} io):".format(task.task_id, t.io, task.io)

        # Check Instances Type
        for key, instance_type in self.env.items():

            types = self.repo.get_instance_type(filter={
                'instance_type': key
            })

            if len(types) == 0:
                # add instance to control database
                self.__add_instance_type_to_database(instance_type)
            else:
                # check instance type consistency
                inst_type_repo = types[0]
                assert inst_type_repo.vcpu == instance_type.vcpu, "Consistency error (vcpu instance {}): " \
                                                                  "{} <> {} ".format(key,
                                                                                     inst_type_repo.vcpu,
                                                                                     instance_type.vcpu)

                assert inst_type_repo.memory == instance_type.memory, "Consistency error (memory instance {}):" \
                                                                      "{} <> {}".format(key,
                                                                                        inst_type_repo.memory,
                                                                                        instance_type.memory)

    def __add_job_to_database(self):
        """Record a Job and its tasks to the control database"""

        job_repo = JobRepo(
            id=self.job.job_id,
            name=self.job.job_name,
            description=self.job.description
        )

        self.repo.add_job(job_repo)

        # add tasks
        for task_id, task in self.job.tasks.items():
            self.repo.add_task(
                TaskRepo(
                    job=job_repo,
                    task_id=task.task_id,
                    command=task.command,
                    memory=task.memory,
                    io=task.io
                )
            )

    def __add_instance_type_to_database(self, instance_type):
        self.repo.add_instance_type(
            InstanceTypeRepo(
                type=instance_type.type,
                vcpu=instance_type.vcpu,
                memory=instance_type.memory,
                provider=instance_type.provider
            )
        )

    '''
    HANDLES FUNCTIONS
    '''

    def __idle_handle(self, dispatcher: Dispatcher):
        self.semaphore.acquire()

        if dispatcher in self.working_dispatchers:

            # To ensure that no one had put a new task during the IDLE EVENT
            if dispatcher.list_of_tasks.size == 0:
                self.working_dispatchers.remove(dispatcher)

                # clean queue
                # dispatcher.reset_queue_structures()

                self.idle_dispatchers.append(dispatcher)

        else:
            dispatcher.waiting_work.set()

        self.semaphore.release()

    def __hibernation_handle(self, dispatcher: Dispatcher):

        self.semaphore.acquire()

        self.working_dispatchers.remove(dispatcher)

        # clean queue
        # dispatcher.reset_queue_structures()

        self.hibernated_dispatcher.append(dispatcher)

        self.semaphore.release()

    def __fault_handle(self, dispatcher: Dispatcher, affected_tasks):
        # Move task to other VM
        # Create a new Dispatcher

        # Create the Vm that will be used by the dispatcher
        vm = VirtualMachine(
            instance_type=dispatcher.vm.instance_type,
            market=dispatcher.vm.market,
            execution_id=self.execution_id,
            volume_id=dispatcher.vm.volume_id,
            job_id=self.job.job_id
        )

        # than a dispatcher, that will receive the tasks, is create
        new_dispatcher = Dispatcher(
            vm=vm,
            queue=None,
            job=self.job,
            execution_id=self.execution_id,
            from_migration=True,
            ckp_barrier=self.ckp_barrier,
            checkpoint_period=self.ckp_period
        )
        # TODO remove comments during normal execution
        # register the new Spot VM to the simulator
        # if self.sim_conf.with_simulation and vm.market == CloudManager.PREEMPTIBLE:
        #     self.simulator.register_vm(vm)

        new_dispatcher.migrate_tasks(affected_tasks)

        self.semaphore.acquire()

        # add new dispatchers to work list
        self.working_dispatchers.append(new_dispatcher)
        new_dispatcher.main_thread.start()
        new_dispatcher.waiting_work.set()

        # remove affected dispatcher from work list
        dispatcher.working = False
        dispatcher.waiting_work.set()
        self.working_dispatchers.remove(dispatcher)
        self.terminated_dispatcher.append(dispatcher)

        self.semaphore.release()

    def __error_handle(self, dispatcher: Dispatcher):
        self.semaphore.acquire()

        if dispatcher in self.working_dispatchers:

            # To ensure that no one had put a new task during the IDLE EVENT
            if dispatcher.list_of_tasks.size == 0:
                self.working_dispatchers.remove(dispatcher)

                # clean queue
                # dispatcher.reset_queue_structures()

                self.terminated_dispatcher.append(dispatcher)

        else:
            dispatcher.waiting_work.set()

        self.semaphore.release()

    def __events_handle(self, event):
        event_timestamp: datetime = datetime.now()

        affected_dispatcher: Dispatcher = event.kwargs['dispatcher']

        # create list of affected tasks
        affected_tasks: List[Task] = event.kwargs["running"]
        affected_tasks.extend(event.kwargs["waiting"])
        affected_tasks.extend(event.kwargs["hibernated"])

        logging.info("Instance: '{}', event: '{}', '{}'".format(
            affected_dispatcher.vm.instance_id,
            event.value,
            event_timestamp))

        if event.value == CloudManager.IDLE:
            logging.info("Calling Idle Handle")
            self.__idle_handle(affected_dispatcher)

        elif event.value == CloudManager.HIBERNATED:
            logging.info("Calling Hibernate Handle")
            self.__hibernation_handle(affected_dispatcher)

        elif event.value in CloudManager.TERMINATED:
            logging.info("Calling Terminate Handle")
            self.__fault_handle(affected_dispatcher, affected_tasks)

        elif event.value in CloudManager.ERROR:
            # ERROR to initiate a new VM.
            self.__error_handle(affected_dispatcher)

    '''
    CHECKERS FUNCTIONS
    '''

    def __check_idle_dispatchers(self):
        """REMOVE DISPATCHERS FROM IDLE POOL"""

        self.semaphore.acquire()

        for dispatcher in self.idle_dispatchers[:]:
            dispatcher.working = False
            dispatcher.waiting_work.set()

            self.idle_dispatchers.remove(dispatcher)
            self.terminated_dispatcher.append(dispatcher)

        self.semaphore.release()

    def __check_hibernated_dispatchers(self):
        self.semaphore.acquire()

        for dispatcher in self.hibernated_dispatcher[:]:

            if dispatcher.vm.state == CloudManager.RUNNING:
                self.hibernated_dispatcher.remove(dispatcher)
                self.working_dispatchers.append(dispatcher)

        self.semaphore.release()

    def __checkers(self):
        # Checker loop
        # Checker if all dispatchers have finished the execution
        while len(self.working_dispatchers) > 0 or len(self.hibernating_dispatcher) > 0:
            # If new checkers would be created that function have to be updated
            self.__check_hibernated_dispatchers()
            self.__check_idle_dispatchers()
            time.sleep(5)

    '''
    Manager Functions
    '''

    def __start_working_dispatchers(self):
        self.semaphore.acquire()

        # Starting working dispatchers
        for dispatcher in self.working_dispatchers:
            dispatcher.main_thread.start()
            dispatcher.waiting_work.set()

        self.semaphore.release()

    def __terminate_dispatchers(self):
        self.semaphore.acquire()

        # Terminate all HIBERNATED DISPATCHERS
        logging.info("Terminating hibernated instances")
        for hibernated_dispatcher in self.hibernated_dispatcher[:]:
            hibernated_dispatcher.working = False
            hibernated_dispatcher.waiting_work.set()

            self.hibernated_dispatcher.remove(hibernated_dispatcher)
            self.terminated_dispatcher.append(hibernated_dispatcher)

        # Terminate all  idle_dispatchers
        logging.info("Terminating idle instances")
        for idle_dispatcher in self.idle_dispatchers[:]:
            idle_dispatcher.working = False
            idle_dispatcher.waiting_work.set()

            self.idle_dispatchers.remove(idle_dispatcher)
            self.terminated_dispatcher.append(idle_dispatcher)

        self.semaphore.release()

        # terminate simulation
        if self.sim_conf.with_simulation:
            self.simulator.stop_simulation()

    def __end_of_execution(self):
        # end of execution
        self.end_timestamp = datetime.now()
        self.elapsed_time = (self.end_timestamp - self.start_timestamp)

        logging.info("Waiting instances to terminate")

        # Waiting all instances terminate
        for terminate_dispatcher in self.terminated_dispatcher:
            terminate_dispatcher.main_thread.join()

        logging.info(
            "Job: {} Execution: {}".format(self.job.job_id, self.execution_id)
        )

        logging.info(
            "Start: {} End: {} Elapsed time: {}".format(self.start_timestamp, self.end_timestamp, self.elapsed_time))

    def start_execution(self):
        # subscriber events_handle
        subscribers.append(self.__events_handle)

        self.start_timestamp = datetime.now()
        # UPDATE DATETIME DEADLINE
        self.deadline_timestamp = self.start_timestamp + timedelta(seconds=self.deadline_seconds)

        self.expected_makespan_timestamp = self.start_timestamp + timedelta(seconds=self.expected_makespan_seconds)

        logging.info("Job: {} Execution: {} Start: {} Expected Makespan: {}  Deadline: {}".format(
            self.job.job_id,
            self.execution_id,
            self.start_timestamp,
            self.expected_makespan_timestamp,
            self.deadline_timestamp)
        )

        self.__start_working_dispatchers()

        self.__checkers()

        self.__terminate_dispatchers()

        self.__end_of_execution()

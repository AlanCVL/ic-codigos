#!/usr/bin/env bash


sudo apt update

sudo apt install -y libpq-dev
sudo apt install -y python3
sudo apt install -y python3-pip
sudo apt install -y postgresql-client

key='AKIA4F4WADPGTEJ6G5WP'
pwd='hKeWa4HQGCZeGbbbdgVsiPcKQ+F5N/xILbydS3L8'
uid=1000
gid=1000
bucket='bucket-p'


mkdir -p $HOME/Devel/
mkdir -p $HOME/.aws/
mkdir -p $HOME/s3
mkdir -p $HOME/efs

export AWS_ACCESS_ID=$key
export AWS_ACCESS_PWD=$pwd

export SETUP_PATH=$HOME/Devel/control/
export SETUP_FILE=setup.cfg


echo "export AWS_ACCESS_ID=$key
export AWS_ACCESS_PWD=$pwd

export SETUP_PATH=$HOME/Devel/control/
export SETUP_FILE=setup.cfg

alias db='psql -h localhost -U postgres -d controldb'
" | tee -a $HOME/.bashrc

alias db='psql -h localhost -U postgres -d controldb'



(cd $HOME/Devel; git clone http://github.com/luanteylo/control; cd $HOME/Devel/control; git checkout checkpoint; pip install -r requirements/prod.txt)
docker start pg-docker
(cd $HOME/Devel/control; python3 client.py clean_db)


#### SETUP S3 - AWS

echo 'Setup s3fs and EC2'
echo "$key:$pwd"| sudo tee -a /etc/passwd-s3fs > /dev/null
sudo chmod 640 /etc/passwd-s3fs
echo "user_allow_other" | sudo tee -a /etc/fuse.conf > /dev/null

echo "[default]
aws_access_key_id=$key
aws_secret_access_key=$pwd
"| tee -a $HOME/.aws/credentials > /dev/null

echo "[default]
region=us-east-1
output=json
" | tee -a $HOME/.aws/config > /dev/null

# mount the bucket
sudo s3fs $bucket -o use_cache=/tmp -o allow_other -o uid=$uid -o gid=$gid -o mp_umask=002 -o multireq_max=5 ~/s3
# sudo mount -t nfs4 -o nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2,noresvport fs-92131b12.efs.us-east-1.amazonaws.com:/ $HOME/efs


echo "Don't forget to send the .pem!!!"
echo "Don't foget to send the update version of synthetic!!!!!"
echo "Don't forget to define app folder!!!"


#sudo apt install docker.io

##  INSTALL GOOGLE CLOUD ENGINE
#
## Create environment variable for correct distribution
#export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
#
## Add the Cloud SDK distribution URI as a package source
#echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
#
## Import the Google Cloud Platform public key
#curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
#
## Update the package list and install the Cloud SDK
#sudo apt update
#sudo apt install google-cloud-sdk

from control.domain.instance_type import InstanceType
from control.scheduler.primary_scheduler import PrimaryScheduler
from control.domain.job import Job

import argparse

import json

from datetime import timedelta

def load(job_file, env_file):
    # load env

    # loading ENV
    with open(env_file) as f:
        env_json = json.load(f)

    env = {}

    for instance in InstanceType.from_dict(env_json):
        env[instance.type] = instance

    # load JOB
    with open(job_file) as f:
        job_json = json.load(f)

    job = Job.from_dict(job_json)

    return job, env


def main():
    parser = argparse.ArgumentParser(description='')

    parser.add_argument('--path', type=str, required=True)
    parser.add_argument('--job_file', type=str, required=True)
    parser.add_argument('--env_file', type=str, required=True)

    args = parser.parse_args()

    job, env = load(args.path + args.job_file, args.path + args.env_file)

    scheduler = PrimaryScheduler(job, env, timedelta(seconds=99999999))

    scheduler.create_map()
    scheduler.write_json(args.path + 'map_viterbo.json')




if __name__ == "__main__":
    main()

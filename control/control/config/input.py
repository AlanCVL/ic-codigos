from control.config.config import Config


class Input(Config):
    _key = 'input'

    @property
    def path(self):
        return self.get_property(self._key, 'path')

    @property
    def job_file(self):
        return self.get_property(self._key, 'job_file')

    @property
    def env_file(self):
        return self.get_property(self._key, 'env_file')

    @property
    def map_file(self):
        return self.get_property(self._key, 'map_file')



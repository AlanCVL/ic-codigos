#!/usr/bin/env python

from control.domain.instance_type import InstanceType
from control.domain.job import Job

from control.generators.input_generator import InputGenerator

from control.scheduler.primary_scheduler import PrimaryScheduler

import argparse

import json

from datetime import timedelta


def load_files(job_file, env_file):
    # load env

    # loading ENV
    with open(env_file) as f:
        env_json = json.load(f)

    env = {}

    for instance in InstanceType.from_dict(env_json):
        env[instance.type] = instance

    # load JOB
    with open(job_file) as f:
        job_json = json.load(f)

    job = Job.from_dict(job_json)

    return job.tasks, env


def __create_env(env_file, csv_file):
    InputGenerator.generate_env_file(csv_file=csv_file, output_file=env_file)


def __create_map(path, env_file, job_file, output_name, deadline, selection_method):
    tasks, env = load_files(path + job_file, path + env_file)

    scheduler = PrimaryScheduler(tasks=tasks,
                                 instances=env,
                                 deadline=timedelta(seconds=float(deadline)),
                                 selection_method=selection_method)

    scheduler.build()
    scheduler.print_map()
    scheduler.write_json(output_name, path)


def main():
    parser = argparse.ArgumentParser(description='Dynamic Scheduling Control Application')

    parser.add_argument('--path', help='Path to the the input files', type=str, required=True)

    parser.add_argument('--job_file', help='Name of the job_file', type=str)
    parser.add_argument('--map_name', help='Name of the map_file', type=str)
    parser.add_argument('--env_file', help='Name of the env_file', type=str)

    parser.add_argument('--deadline', type=int, required=True)

    list_of_choices = ['round-robin', 'min_price']

    parser.add_argument('--selection', help="Select the strategy of VM's selection", choices=list_of_choices,
                        default='round-robin')

    args = parser.parse_args()

    if args.map_name is None or args.env_file is None or args.job_file is None:
        raise ValueError("Files not found...")

    deadline = args.deadline

    print("Creating map file...")
    print("File name: {}".format(args.map_name))
    print("Selection method: {}".format(args.selection))

    __create_map(path=args.path, env_file=args.env_file, job_file=args.job_file, output_name=args.map_name,
                 deadline=deadline, selection_method=args.selection)


if __name__ == "__main__":
    main()

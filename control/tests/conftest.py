# import pytest
#
# from control.config.ec2_config import EC2Config
# from control.managers.ec2_manager import EC2Manager
#
#
# def pytest_addoption(parser):
#     parser.addoption('--launch', action='store_true',
#                      help='run launch test')
#     parser.addoption('--terminate', action='store_true',
#                      help='run terminate test')
#     parser.addoption('--manager', action='store_true',
#                      help='run manager tests')
#
#
# def pytest_runtest_setup(item):
#     if 'launch' in item.keywords and not item.config.getvalue('launch'):
#         pytest.skip('need --launch option to run')
#
#     if 'terminate' in item.keywords and not item.config.getvalue('terminate'):
#         pytest.skip('need --terminate option to run')
#
#     if 'manager' in item.keywords and not item.config.getvalue('manager'):
#         pytest.skip('need --manager option to run')
#
#
# @pytest.fixture
# def instance_info():
#     config = EC2Config()
#
#     return {
#         'instance_type': 't2.micro',
#         'image_id': config.image_id,
#         'key_name': config.key_name,
#         'instance_id': {
#             ' i-0ba4f93992ad52923 ',
#             ' i-019bd4a0a18c7ea6c ',
#             ' i-02794cf8179c58dd0 ',
#             ' i-0f7699192fb67c5c2 ',
#             ' i-0d1daae5f274a19ce '
#         }
#     }
#
#
# @pytest.fixture
# def job_info():
#     return {
#         "job_id": 20,
#         "job_name": "Simple job",
#         "tasks": {
#             "0": {
#                 "memory": 1.49,
#                 "io": 0.98,
#                 "command": "sleep 5",
#                 "runtime": {
#                     "c3.xlarge": 5,
#                     "c3.large": 5
#                 }
#             },
#             "1": {
#                 "memory": 0.5,
#                 "io": 0.0,
#                 "command": "sleep 10",
#                 "runtime": {
#                     "c3.xlarge": 5,
#                     "c3.large": 5
#                 }
#             }
#         }
#     }
#
#
# @pytest.fixture(scope='session')
# def handle():
#     return EC2Manager()

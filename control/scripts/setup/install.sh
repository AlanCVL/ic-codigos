sudo apt update


sudo apt install -y python3-pip
sudo apt install -y libpq-dev

echo "install docker"
(cd docker; sh setup_docker.sh)

echo "install S3"
(cd s3; sh setup_s3.sh)

echo "install postgres"
(cd ami; sh install_docker_postgres.sh; sh setup_hibernation.sh)

echo "install control"
(cd control; sh install_control.sh)

echo "rebooting system..."

sudo reboot

from control.domain.instance_type import InstanceType
from control.domain.task import Task

from control.managers.cloud_manager import CloudManager
from control.managers.virtual_machine import VirtualMachine


def test_virtual_machine():
    instance = InstanceType(
        provider=CloudManager.EC2,
        instance_type='t2.nano',
        memory=0.5,
        vcpu=1,
        restrictions={'on-demand': 1,
                      'preemptible': 1},
        gflops=0.0
    )

    task = Task(
        job_id=102,
        task_id=2,
        memory=0.2,
        command="sh exec.sh 400",
        io=0,
        runtime={'t2.nano': 100}
    )

    vm = VirtualMachine(
        instance_type=instance,
        market='on-demand',
        execution_id=0
    )

    vm.instance_id = 'i-092d66eb884f2930e'

    vm.prepare_vm()

    # vm.execute_task(task)
    print(vm.check_task(task))

#!/usr/bin/env python3
from datetime import datetime
from datetime import timedelta

import argparse
import os
import subprocess
import re

import json
import io
import struct

import sys
import socket
import selectors
import traceback

import shutil

import logging

CHECKPOINT_LIMIT = 3


class Daemon:
    START = 'start'
    STATUS = 'status'
    CHECKPOINT_STOP = 'checkpoint_stop'
    RESTART = 'restart'
    STOP = 'stop'
    CHECKPOINT = 'checkpoint'
    USAGE = 'usage'

    TEST = 'test'

    def __init__(self, root_path, work_path, container_image, job_id, execution_id, instance_id):

        self.work_path = work_path
        self.container_image = container_image

        self.job_id = job_id
        self.execution_id = execution_id
        self.instance_id = instance_id

        self.root_path = root_path + "{}_{}/".format(self.job_id, self.execution_id)

        self.__prepare_logging()

    # waiting for commands

    def __prepare_logging(self):

        logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
        rootLogger = logging.getLogger()
        rootLogger.setLevel('INFO')

        file_name = self.root_path + "{}_{}_{}.log".format(self.job_id, self.execution_id, self.instance_id)

        fileHandler = logging.FileHandler(file_name)
        fileHandler.setFormatter(logFormatter)
        rootLogger.addHandler(fileHandler)

        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logFormatter)
        rootLogger.addHandler(consoleHandler)

    def handle_command(self, action, value):

        task_id = value['task_id']
        command = value['command']

        container_name = "container_{}_{}_{}".format(
            self.job_id,
            self.execution_id,
            task_id
        )

        data_path = self.root_path + "{}/data/".format(task_id)
        backup_path = self.root_path + "{}/backup/".format(task_id)

        logging.info("Container {}: Action {}".format(container_name, action))

        operation_time = timedelta(seconds=0)

        if action == Daemon.START:
            start_time = datetime.now()
            # create a new container
            try:
                self.__create_container(data_path=data_path,
                                        container_name=container_name,
                                        container_cmd=command)

                self.__start_container(container_name=container_name)

                status_return = "success"
                value_return = "container '{}' create with sucess".format(container_name)

            except Exception as e:
                logging.error(e)
                status_return = "error"
                value_return = "Error to create and start container '{}'".format(container_name)

            end_time = datetime.now()

            operation_time = end_time - start_time

        elif action == Daemon.STATUS:
            try:

                value_return = self.__get_container_status(container_name)
                status_return = "success"
            except Exception as e:
                logging.error(e)
                value_return = "Error to get container {} status".format(container_name)
                status_return = "error"

        elif action == Daemon.CHECKPOINT_STOP:
            start_time = datetime.now()

            # Try to create the backup_path
            cmd_folder = "mkdir -p {}".format(backup_path)
            subprocess.run(cmd_folder.split())

            try:
                checkpoint_return = self.__checkpoint(container_name, backup_path)
                stop_return = self.___stop_container(container_name)

                value_return = {"checkpoint": checkpoint_return, "stop": stop_return}

                status_return = "success"
            except Exception as e:
                logging.error(e)
                value_return = "Error to checkpoint and stop container {} status".format(container_name)
                status_return = "error"

            end_time = datetime.now()

            operation_time = end_time - start_time

        elif action == Daemon.RESTART:
            start_time = datetime.now()

            # Try to create the backup_path
            cmd_folder = "mkdir -p {}".format(backup_path)
            subprocess.run(cmd_folder.split())

            try:
                value_return = self.__restart_from_last_checkpoint(container_name, data_path, command, backup_path)
                status_return = "success"
            except Exception as e:
                value_return = "Error to restart container {} status".format(container_name)
                status_return = "error"
                logging.error(e)

            end_time = datetime.now()

            operation_time = end_time - start_time

        elif action == Daemon.STOP:
            start_time = datetime.now()

            try:
                value_return = self.___stop_container(container_name)
                status_return = "success"
            except Exception as e:
                logging.error(e)
                value_return = "Error stop container {} status".format(container_name)
                status_return = "error"

            end_time = datetime.now()

            operation_time = end_time - start_time

        elif action == Daemon.CHECKPOINT:
            start_time = datetime.now()

            # Try to create the backup_path
            cmd_folder = "mkdir -p {}".format(backup_path)
            subprocess.run(cmd_folder.split())

            try:
                value_return = self.__checkpoint(container_name, backup_path)
                status_return = "success"
            except Exception as e:
                logging.error(e)
                value_return = "Error to checkpoint container {} status".format(container_name)
                status_return = "error"

            end_time = datetime.now()

            operation_time = end_time - start_time

        elif action == Daemon.USAGE:
            try:

                value_return = self.__get_container_usage(container_name)
                status_return = "success"
            except Exception as e:
                logging.error(e)
                value_return = "Error to get container {} usage".format(container_name)
                status_return = "error"

        elif action == Daemon.TEST:
            value_return = "Hello world"
            status_return = "success"

        else:
            value_return = "invalid command"
            status_return = "error"

        logging.info(str({"status": status_return, "value": value_return, "duration": str(operation_time)}))

        return {"status": status_return, "value": value_return}

    def __get_container_usage(self, container_name):
        # check container status
        cmd = "docker container stats {} --no-stream".format(
            container_name
        )
        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        out, err = process.communicate()

        line = out.decode().splitlines()[-1]

        cpu_usage = line.split()[1]
        memory = line.split()[2]
        memory_index = line.split()[3]

        return {
            "memory": memory + " " + memory_index,
            "cpu": cpu_usage

        }

    def __get_container_status(self, container_name):

        # check container status
        cmd = "docker container inspect -f '{{{{.State.Status}}}}' {}".format(
            container_name
        )

        process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
        out, err = process.communicate()
        out = out.rstrip()

        status = out.decode().replace("'", "")

        exit_code = None

        if status == 'exited':
            # get exit code
            cmd = "docker container inspect -f '{{{{.State.ExitCode}}}}' {}".format(
                container_name
            )
            process = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE)
            out, err = process.communicate()
            out = out.rstrip()
            exit_code = out.decode().replace("'", "")
        elif status == '':
            status = "not found"

        return {"status": status, "exit_code": exit_code}

    def __restart_from_last_checkpoint(self, container_name, data_path, container_cmd, backup_path):

        # execute command to load the s3 directory
        os.listdir(backup_path)

        status = self.__get_container_status(container_name)["status"]

        if status == "not found":
            self.__create_container(data_path=data_path,
                                    container_name=container_name,
                                    container_cmd=container_cmd)

        checkpoint_list = self.__get_checkpoint_list(backup_path)

        if checkpoint_list is not None:

            created = False

            for checkpoint_name in checkpoint_list:
                cmd = 'docker start --checkpoint={} --checkpoint-dir={} {}'.format(
                    checkpoint_name,
                    backup_path,
                    container_name
                )

                logging.info(cmd)

                subprocess.run(cmd.split())

                # check if checkpoint was created with success
                info = self.__get_container_status(container_name)
                if info['status'] == 'running':
                    created = True
                    break

            # check if recovery was created with success
            if not created:
                logging.error('Error to recovery container {} from checkpoint'.format(container_name))
                raise Exception('Error to recovery container from checkpoint')

            msg = "container {} restarted with success".format(container_name)

        else:
            # container don't have a checkpoint
            self.__start_container(container_name)
            msg = "container {} started without checkpoint".format(container_name)

        logging.info(msg)

        return msg

    def __checkpoint(self, container_name, backup_path):
        status = self.__get_container_status(container_name)["status"]

        # regex to extract checkpoint number from checkpoint file
        re_checkpoint = "[^0-9]*([0-9]*)"

        cpcount = 0

        code = None
        err = ""
        out = ""

        if status == "running":

            checkpoint_list = self.__get_checkpoint_list(backup_path)

            if checkpoint_list is not None:
                newest = checkpoint_list[0]
                m = re.search(re_checkpoint, newest)
                cpcount = int(m.group(1))
                cpcount = (cpcount + 1) % CHECKPOINT_LIMIT

            checkpoint_name = 'checkpoint{}'.format(cpcount)

            # If checkpoint already exist, we have to delete it
            if os.path.isdir(backup_path + checkpoint_name):
                rm_cmd = "sudo rm -rf {}".format(os.path.join(backup_path, checkpoint_name))
                logging.info(rm_cmd)
                subprocess.run(rm_cmd.split())
                # shutil.rmtree(backup_path + checkpoint_name)

            cmd1 = "docker checkpoint create --checkpoint-dir={} --leave-running=true {} {}".format(
                backup_path,
                container_name,
                checkpoint_name)

            logging.info(cmd1)

            r = subprocess.run(cmd1.split())

            if r.returncode != 0:
                err = r.stderr
                out = r.stdout
                code = r.returncode

                msg = "Checkpoint of container {} Stderr: {} stdout: {} Code: {}".format(container_name, err, out, code)

            else:
                msg = "Checkpoint of container {} created with success".format(container_name)

        else:
            msg = "Checkpoint Error - Container {}  is not running".format(container_name)

        return {"msg": msg, "code": code, "out": out, "err": err}

    def ___stop_container(self, container_name):

        code = ""
        err = ""
        out = ""

        operation_time = timedelta(seconds=0.0)

        status = self.__get_container_status(container_name)["status"]

        if status == 'running':

            cmd = "docker stop {}".format(container_name)

            logging.info(cmd)

            start_time = datetime.now()
            r = subprocess.run(cmd.split())
            end_time = datetime.now()

            operation_time = end_time - start_time

            if r.returncode != 0:
                msg = "Error to Stop Container {}".format(container_name)
                code = r.returncode
                out = r.stdout
                err = r.stderr
            else:
                msg = "Container {} stopped with success".format(container_name)

        else:
            msg = "Container {} is not running".format(container_name)

        return {"msg": msg, "code": code, "err": err, "out": out, "duration": str(operation_time)}

    def __create_container(self, data_path, container_name, container_cmd):
        # def create_container(container_name, volume_path, workdir, container_image, container_cmd):
        # check if container exist

        exist = True

        try:
            subprocess.check_output('docker container inspect {}'.format(container_name),
                                    shell=True)
        except:
            exist = False

        if not exist:
            # create containe
            cmd = 'docker create --name {} -v {}:{} -w {} {} {}'.format(
                container_name, data_path, self.work_path, self.work_path, self.container_image, container_cmd)

            logging.info(cmd)

            subprocess.run(cmd.split())

    def __start_container(self, container_name):
        # start docker without checkpoint
        cmd = 'docker start {}'.format(
            container_name
        )

        logging.info(cmd)

        subprocess.run(cmd.split())

    def __get_checkpoint_list(self, backup_path):

        os.chdir(backup_path)

        checkpoint_list = sorted(os.listdir(os.getcwd()), key=os.path.getmtime, reverse=True)

        logging.info('checkpoint list size: {}'.format(len(checkpoint_list)))

        if len(checkpoint_list) > 0:
            return checkpoint_list

        return None


class Message:
    daemon: Daemon

    def __init__(self, selector, sock, addr):
        self.selector = selector
        self.sock = sock
        self.addr = addr
        self._recv_buffer = b""
        self._send_buffer = b""
        self._jsonheader_len = None
        self.jsonheader = None
        self.request = None
        self.response_created = False

        self.daemon = None

    def _set_selector_events_mask(self, mode):
        """Set selector to listen for events: mode is 'r', 'w', or 'rw'."""
        if mode == "r":
            events = selectors.EVENT_READ
        elif mode == "w":
            events = selectors.EVENT_WRITE
        elif mode == "rw":
            events = selectors.EVENT_READ | selectors.EVENT_WRITE
        else:
            raise ValueError(f"Invalid events mask mode {repr(mode)}.")
        self.selector.modify(self.sock, events, data=self)

    def _read(self):
        try:
            # Should be ready to read
            data = self.sock.recv(4096)
        except BlockingIOError:
            # Resource temporarily unavailable (errno EWOULDBLOCK)
            pass
        else:
            if data:
                self._recv_buffer += data
            else:
                raise RuntimeError("Peer closed.")

    def _write(self):
        if self._send_buffer:
            print("sending", repr(self._send_buffer), "to", self.addr)
            try:
                # Should be ready to write
                sent = self.sock.send(self._send_buffer)
            except BlockingIOError:
                # Resource temporarily unavailable (errno EWOULDBLOCK)
                pass
            else:
                self._send_buffer = self._send_buffer[sent:]
                # Close when the buffer is drained. The response has been sent.
                if sent and not self._send_buffer:
                    self.close()

    def _json_encode(self, obj, encoding):
        return json.dumps(obj, ensure_ascii=False).encode(encoding)

    def _json_decode(self, json_bytes, encoding):
        tiow = io.TextIOWrapper(
            io.BytesIO(json_bytes), encoding=encoding, newline=""
        )
        obj = json.load(tiow)
        tiow.close()
        return obj

    def _create_message(
        self, *, content_bytes, content_type, content_encoding
    ):
        jsonheader = {
            "byteorder": sys.byteorder,
            "content-type": content_type,
            "content-encoding": content_encoding,
            "content-length": len(content_bytes),
        }
        jsonheader_bytes = self._json_encode(jsonheader, "utf-8")
        message_hdr = struct.pack(">H", len(jsonheader_bytes))
        message = message_hdr + jsonheader_bytes + content_bytes
        return message

    def _create_response_json_content(self):
        action = self.request.get("action")

        value = self.request.get("value")

        answer = self.daemon.handle_command(action, value)

        content = {"result": answer}

        content_encoding = "utf-8"
        response = {
            "content_bytes": self._json_encode(content, content_encoding),
            "content_type": "text/json",
            "content_encoding": content_encoding,
        }

        return response

    def _create_response_binary_content(self):
        response = {
            "content_bytes": b"First 10 bytes of request: "
                             + self.request[:10],
            "content_type": "binary/custom-server-binary-type",
            "content_encoding": "binary",
        }
        return response

    def process_events(self, mask):
        if mask & selectors.EVENT_READ:
            self.read()
        if mask & selectors.EVENT_WRITE:
            self.write()

    def read(self):
        self._read()

        if self._jsonheader_len is None:
            self.process_protoheader()

        if self._jsonheader_len is not None:
            if self.jsonheader is None:
                self.process_jsonheader()

        if self.jsonheader:
            if self.request is None:
                self.process_request()

    def write(self):
        if self.request:
            if not self.response_created:
                self.create_response()

        self._write()

    def close(self):
        print("closing connection to", self.addr)
        try:
            self.selector.unregister(self.sock)
        except Exception as e:
            print(
                f"error: selector.unregister() exception for",
                f"{self.addr}: {repr(e)}",
            )

        try:
            self.sock.close()
        except OSError as e:
            print(
                f"error: socket.close() exception for",
                f"{self.addr}: {repr(e)}",
            )
        finally:
            # Delete reference to socket object for garbage collection
            self.sock = None

    def process_protoheader(self):
        hdrlen = 2
        if len(self._recv_buffer) >= hdrlen:
            self._jsonheader_len = struct.unpack(
                ">H", self._recv_buffer[:hdrlen]
            )[0]
            self._recv_buffer = self._recv_buffer[hdrlen:]

    def process_jsonheader(self):
        hdrlen = self._jsonheader_len
        if len(self._recv_buffer) >= hdrlen:
            self.jsonheader = self._json_decode(
                self._recv_buffer[:hdrlen], "utf-8"
            )
            self._recv_buffer = self._recv_buffer[hdrlen:]
            for reqhdr in (
                "byteorder",
                "content-length",
                "content-type",
                "content-encoding",
            ):
                if reqhdr not in self.jsonheader:
                    raise ValueError(f'Missing required header "{reqhdr}".')

    def process_request(self):
        content_len = self.jsonheader["content-length"]
        if not len(self._recv_buffer) >= content_len:
            return
        data = self._recv_buffer[:content_len]
        self._recv_buffer = self._recv_buffer[content_len:]
        if self.jsonheader["content-type"] == "text/json":
            encoding = self.jsonheader["content-encoding"]
            self.request = self._json_decode(data, encoding)
            print("received request", repr(self.request), "from", self.addr)
        else:
            # Binary or unknown content-type
            self.request = data
            print(
                f'received {self.jsonheader["content-type"]} request from',
                self.addr,
            )
        # Set selector to listen for write events, we're done reading.
        self._set_selector_events_mask("w")

    def create_response(self):
        if self.jsonheader["content-type"] == "text/json":
            response = self._create_response_json_content()
        else:
            # Binary or unknown content-type
            response = self._create_response_binary_content()
        message = self._create_message(**response)
        self.response_created = True
        self._send_buffer += message


def prepare_folders(path, own):
    st = os.stat(path)
    uid = st.st_uid

    if uid != own:
        for root, dirs, files in os.walk(path):
            for file in files:
                os.chmod(root + "/" + file, 0o775)
            os.chmod(root, 0o775)
            os.chown(root, own, own)


sel = selectors.DefaultSelector()


def accept_wrapper(sock):
    conn, addr = sock.accept()  # Should be ready to read
    print("accepted connection from", addr)
    conn.setblocking(False)
    message = Message(sel, conn, addr)
    sel.register(conn, selectors.EVENT_READ, data=message)


def main():
    parser = argparse.ArgumentParser(description='Execute docker image with checkpoint record.')

    parser.add_argument('--root_path', type=str, required=True)
    parser.add_argument('--work_path', type=str, required=True)
    parser.add_argument('--container_image', type=str, required=True)
    parser.add_argument('--job_id', type=int, required=True)
    parser.add_argument('--execution_id', type=int, required=True)
    parser.add_argument('--instance_id', type=str, required=True)

    args = parser.parse_args()

    # create a daemon

    daemon = Daemon(
        root_path=args.root_path,
        work_path=args.work_path,
        container_image=args.container_image,
        job_id=args.job_id,
        execution_id=args.execution_id,
        instance_id=args.instance_id
    )

    host = '0.0.0.0'
    port = 8080

    lsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Avoid bind() exception: OSError: [Errno 48] Address already in use
    lsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    lsock.bind((host, port))
    lsock.listen()
    print("listening on", (host, port))
    lsock.setblocking(False)
    sel.register(lsock, selectors.EVENT_READ, data=None)

    try:
        while True:
            events = sel.select(timeout=None)
            for key, mask in events:
                if key.data is None:
                    accept_wrapper(key.fileobj)
                else:
                    message = key.data
                    message.daemon = daemon
                    try:
                        message.process_events(mask)
                    except Exception:
                        print(
                            "main: error: exception for",
                            f"{message.addr}:\n{traceback.format_exc()}",
                        )
                        message.close()
    except KeyboardInterrupt:
        print("caught keyboard interrupt, exiting")
    finally:
        sel.close()


if __name__ == "__main__":
    main()

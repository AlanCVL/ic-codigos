

# Criar um novo container
docker create --name test_exec -v /home/ubuntu/exec:/home/worker/ -w /home/worker/ synthetic sh exec.sh 100

# Inicia container
docker start test_exec


# LOOP de 5 minutos.
# Gravar o checkpoint
docker checkpoint create --checkpoint-dir=/home/ubuntu/exec/checkpoint/ --leave-running=true test_exec checkpoint1

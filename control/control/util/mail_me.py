import smtplib
from datetime import datetime


class MailMe:

    def __init__(self, user, pwd):

        self.user = user
        self.pwd = pwd

    def __prepare_and_send(self, recipient, subject, body):

        FROM = self.user
        TO = recipient if type(recipient) is list else [recipient]
        SUBJECT = subject
        TEXT = body

        # Prepare actual message
        message = """From: %s\nTo: %s\nSubject: %s\n\n%s \n\n\n Time: %s
		""" % (FROM, ", ".join(TO), SUBJECT, TEXT, str(datetime.now()))

        try:
            server = smtplib.SMTP("smtp.gmail.com", 587)
            server.ehlo()
            server.starttls()
            server.login(self.user, self.pwd)
            server.sendmail(FROM, TO, message)
            server.close()
            print("successfully sent the mail")
        except:
            print("failed to send mail")

    def send_email(self, recipient, subject, body):

        self.__prepare_and_send(recipient, subject, body)

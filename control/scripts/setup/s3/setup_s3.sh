
sudo apt install -y awscli
sudo apt install -y automake autotools-dev fuse g++ git libcurl4-gnutls-dev libfuse-dev libssl-dev libxml2-dev make pkg-config


# prepare s3 setup
git clone https://github.com/s3fs-fuse/s3fs-fuse.git

(cd s3fs-fuse; ./autogen.sh; ./configure --prefix=/usr --with-openssl; make; sudo make install)

# configuring access key
echo "Access key ID: "
read key
echo "Secret access Key: "
read pwd

echo "$key:$pwd"| sudo tee -a /etc/passwd-s3fs > /dev/null
# echo "$key:$pwd" > /etc/passwd-s3fs
sudo chmod 640 /etc/passwd-s3fs

echo 'Setup s3fs'
echo "user_allow_other" | sudo tee -a /etc/fuse.conf > /dev/null

#echo "Enter uid"vi
#read uid
#echo "Enter gid"
#read gid
#echo "Enter bucket name"
#read bucket

## create the folder to mount the s3 bucket
#mkdir -p $HOME/s3
#
#
## mount the bucket
#sudo s3fs $bucket -o use_cache=/tmp -o allow_other -o uid=$uid -o gid=$gid -o mp_umask=002 -o multireq_max=5 ~/s3
#
## make the bucket permanently
#echo "s3fs#$bucket $HOME/s3 fuse _netdev,allow_other,umask=0007,uid=$uid,gid=$gid,use_cache=/tmp,nonempty 0 0" | sudo tee -a /etc/fstab > /dev/null



#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = [
    # TODO: put package requirements here
]

test_requirements = [
    # TODO: put package test requirements here
]

setup(
    name='config',
    version='0.1.0',
    description="A config package used in the dynamic scheduling project",
    long_description=readme + '\n\n' + history,
    author="Luan Teylo",
    author_email='luanteylo@gmail.com',
    url='https://github.com/luanteylo/config',
    packages=[
        'config',
    ],
    package_dir={'config':
                 'config'},
    include_package_data=True,
    install_requires=requirements,
    license="MIT license",
    zip_safe=False,
    keywords='config',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 2',
        'Programming Language :: Python :: 2.2',
        'Programming Language :: Python :: 2.4',
        'Programming Language :: Python :: 2.5',
    ],
    test_suite='tests',
    tests_require=test_requirements
)

from datetime import timedelta

from typing import Dict

import statistics


class Task:
    EXECUTING = 'executing'
    FINISHED = 'finished'
    WAITING = 'waiting'
    ERROR = 'error'
    RUNTIME_ERROR = 'runtime_error'
    MIGRATED = 'migrated'
    HIBERNATED = 'hibernated'

    RESTARTED = 'restarted'

    def __init__(self, job_id, task_id, memory, command, io, runtime):
        self.job_id = job_id
        self.task_id = task_id
        self.memory = memory
        self.command = command
        self.io = io
        self.runtime: Dict[str, float] = runtime

        self.expected_start = None
        self.expected_end = None
        self.task_size = None

        self.checkpoint_period = None
        self.has_checkpoint = False

    @classmethod
    def from_dict(cls, adict):
        """return a list of tasks created from a dict"""

        return [
            cls(
                job_id=adict['job_id'],
                task_id=int(task_id),
                memory=adict['tasks'][task_id]['memory'],
                io=adict['tasks'][task_id]['io'],
                command=adict['tasks'][task_id]['command'],
                runtime=adict['tasks'][task_id]['runtime']
            )
            for task_id in adict['tasks']
        ]

    def __str__(self):
        return "Job_id:{}, task_id: {}, memory:{}, " \
               "io:{}, command:{}, start:{}, end:{}".format(
            self.job_id, self.task_id,
            self.memory, self.io, self.command,
            self.expected_start, self.expected_end
        )

    def get_runtime(self, instance_type):

        if instance_type in self.runtime:
            return self.runtime[instance_type]
        else:
            return None

    def update_expected_execution_time(self, start, end):
        self.expected_start = start
        self.expected_end = end
        self.task_size = timedelta(end - start)

    def __eq__(self, other):
        return self.task_id == other.task_id

    def __lt__(self, other):
        """
        :type other: Executor
        """
        return self.expected_start < other.expected_start

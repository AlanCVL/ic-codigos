wget https://deb.sipwise.com/debian/pool/main/libt/libtool/libltdl7_2.4.6-6_amd64.deb
sudo dpkg -i libltdl7_2.4.6-6_amd64.deb

wget https://download.docker.com/linux/ubuntu/dists/xenial/pool/stable/amd64/docker-ce_17.03.2~ce-0~ubuntu-xenial_amd64.deb
sudo dpkg -i docker-ce_17.03.2~ce-0~ubuntu-xenial_amd64.deb



sudo apt install -y build-essential
sudo apt install -y gcc
sudo apt install -y libprotobuf-dev libprotobuf-c0-dev protobuf-c-compiler protobuf-compiler python-protobuf
sudo apt install -y pkg-config python-ipaddr iproute2 libcap-dev  libnl-3-dev libnet-dev --no-install-recommends

sudo apt install -y git

git clone https://github.com/checkpoint-restore/criu
cd criu  && make

sudo apt install -y asciidoc  xmlto
sudo make install

sudo groupadd docker
sudo usermod -aG docker $USER

sudo echo '{"experimental": true}'  | sudo tee -a /etc/docker/daemon.json > /dev/null

sudo service docker restart


echo "Build synthetic docker image"

docker build -t synthetic .


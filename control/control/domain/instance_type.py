class InstanceType:

    def __init__(self, provider, instance_type, memory, vcpu, restrictions):
        self.provider = provider
        self.type = instance_type
        self.memory = float(memory) * 1024.0  # GB to MB
        self.vcpu = vcpu
        self.price_ondemand = 0.0
        self.price_preemptible = 0.0
        self.restrictions = restrictions
        self.id = None

        self.region = None
        self.zone = None

    def setup_ondemand_price(self, price, region):
        self.price_ondemand = price
        self.region = region

    def setup_preemptible_price(self, price, region, zone):
        self.price_preemptible = price
        self.region = region
        self.zone = zone

    @classmethod
    def from_dict(cls, adict):
        return [
            cls(
                provider=adict['instances'][key]['provider'],
                instance_type=key,
                memory=adict['instances'][key]['memory'],
                vcpu=adict['instances'][key]['vcpu'],
                restrictions=adict['instances'][key]['restrictions']
            )
            for key in adict['instances']
        ]

    @property
    def market_ondemand(self):
        return self.restrictions['markets']['on-demand']

    @property
    def market_preemptible(self):
        return self.restrictions['markets']['preemptible']

    @property
    def limits_ondemand(self):
        return self.restrictions['limits']['on-demand']

    @property
    def limits_preemptible(self):
        return self.restrictions['limits']['preemptible']

    def __str__(self):
        return "'{}' Memory: '{}' vcpu:'{}' on-demand price: '{}' preemptible price: '{}' " \
               "region: '{}' zone: '{}' ".format(
            self.type,
            self.memory,
            self.vcpu,
            self.price_ondemand,
            self.price_preemptible,
            self.region,
            self.zone
        )
